public class Food
{
    private String description;
    private int calories;
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    @Override
    public String toString()
    {
        return "Somebody brought " + getDescription();
    }
    
    /** Set description for Food object
    *@param inDescription the new description
    */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    public int getCalories()
    {
        return calories;
    }
    
    public void setCalories(int inCalories)
    {
        calories = inCalories;
    }
}